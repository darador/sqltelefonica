drop table PROVINCIAS;

create table PROVINCIAS (

PROV_ID integer NOT NULL identity, 
PRO_DESCRIPCION varchar(50),
primary key (PROV_ID)
);

create table PARTIDOS (

PAR_ID integer NOT NULL identity,
PRO_ID integer,
PAR_DESCRIPCION varchar(50),

primary key (PAR_ID),
foreign key (PRO_ID) references PROVINCIAS (PROV_ID),
);

