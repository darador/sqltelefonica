use db_ruben;

drop table pantallas;
drop table perfiles;

create table pantallas(
pan_ID integer NOT NULL identity, 
pan_DESCRIPCION varchar(50),
primary key (pan_ID)
);

create table perfiles (
per_ID integer NOT NULL identity, 
per_DESCRIPCION varchar(50)
primary key (per_ID)
);

create table perfiles_pantallas (
per_ID integer NOT NULL, 
pan_ID integer NOT NULL,
foreign key (per_ID) REFERENCES PERFILES (per_ID),
foreign key (pan_ID) REFERENCES pantallas (pan_ID),
primary key (per_ID,pan_ID)
);