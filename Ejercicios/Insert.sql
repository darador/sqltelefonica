USE db_ruben;

insert into PROVINCIAS (PRO_DESCRIPCION) values ('Buenos Aires');
insert into PROVINCIAS (PRO_DESCRIPCION) values ('Cordoba');
insert into PROVINCIAS (PRO_DESCRIPCION) values ('Entre rios');
insert into PROVINCIAS (PRO_DESCRIPCION) values ('Mendoza');

insert into PARTIDOS (PRO_ID, PAR_DESCRIPCION) values (1,'Mor�n');
insert into PARTIDOS (PRO_ID, PAR_DESCRIPCION) values (2,'Cordoba Capital');
insert into PARTIDOS (PRO_ID, PAR_DESCRIPCION) values (3,'Parana');
insert into PARTIDOS (PRO_ID, PAR_DESCRIPCION) values (4,'Mendoza Capital');
